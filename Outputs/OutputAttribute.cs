using System;

namespace pab.Outputs
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class OutputAttribute : ProcessorAttribute
    {
        public OutputAttribute (string name)
            :base(name)
        {
        }
    }
}

