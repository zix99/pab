using System;
using System.IO;
using pab.Lib.Image;

namespace pab.Outputs
{
    [Output("dir", Description = "Output frames to a directory. (path)")]
    public class FrameOutput : IOutput
    {
        private readonly string _path;
        private readonly string _ext;

        public FrameOutput (FilterArgs args)
        {
            _path = args[0];
            _ext = args.GetArg<string> (1, "png");
        }

        #region IOutput implementation

        public void Init (Context context)
        {
            Directory.CreateDirectory (_path);
        }

        public void AddFrame (int frame, IImage image)
        {
            string filename = Path.Combine (_path, string.Format ("{0:D5}.{1}", frame, _ext));
            var raw = image.CreateImage ();
            raw.Save (filename);
        }

        public void Finish ()
        {

        }

        #endregion

        public override string ToString ()
        {
            return string.Format ("[FrameOutput {0}]", _path);
        }
    }
}

