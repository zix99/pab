using System;
using pab.Lib.Image;

namespace pab.Outputs
{
    [Output("sprite", Description = "Outputs to a spritesheet of a given name")]
    public class SpritesheetOutput : IOutput
    {
        private readonly string _filename;

        private RawImage _image;
        private int _axisSize;
        private int _frameSize;

        public SpritesheetOutput (FilterArgs args)
        {
            _filename = args [0];
        }


        #region IOutput implementation
        public void Init (Context context)
        {
            _axisSize = (int)Math.Ceiling (Math.Sqrt (context.FrameCount));
            _frameSize = context.FrameSize;

            int finalSize = _axisSize * _frameSize;

            _image = new RawImage (finalSize, finalSize);
        }
        public void AddFrame (int frame, IImage image)
        {
            int xOff = (frame % _axisSize) * _frameSize;
            int yOff = (frame / _axisSize) * _frameSize;

            _image.Blit (image, xOff, yOff);
        }
        public void Finish ()
        {
            _image.Save (_filename);
        }
        #endregion

        public override string ToString ()
        {
            return string.Format ("[SpritesheetOutput: {0}]", _filename);
        }
    }
}

