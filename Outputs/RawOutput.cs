using System;
using pab.Lib.Image;
using System.IO;

namespace pab.Outputs
{
    [Output("raw", Description = "Saves animation as raw internal intermediate file")]
    public class RawOutput : IOutput
    {

        private readonly string _filename;

        public RawOutput (FilterArgs args)
        {
            _filename = args [0];
        }

        private BinaryWriter _writer;

        #region IOutput implementation
        public void Init (Context context)
        {
            var stream = File.OpenWrite (_filename);
            _writer = new BinaryWriter (stream);

            _writer.Write ("PABRAW");
            _writer.Write (context.FrameCount);
        }

        public void AddFrame (int frame, IImage image)
        {
            var raw = image.CreateImage ();
            _writer.Write (frame);
            raw.WriteRaw (_writer.BaseStream);
        }

        public void Finish ()
        {
            _writer.Dispose ();
        }
        #endregion
    }
}

