using System;

namespace pab.Outputs
{
    public interface IOutput
    {
        void Init (Context context);

        void AddFrame(int frame, IImage image);

        void Finish();
    }
}

