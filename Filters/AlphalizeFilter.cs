using System;
using pab.Lib.Image;

namespace pab.Filters
{
    [Filter("alphalize", Description = "Converts the grayscale version of the image into the alpha channel")]
    public class AlphalizeFilter : IFilter
    {
        public AlphalizeFilter (FilterArgs args)
        {
        }

        #region IFilter implementation

        public IImage Filter (Context context, IImage src, int frame)
        {
            var img = src.CreateImage ();

            for (int y=0; y<img.Height; ++y)
            {
                for (int x=0; x<img.Width; ++x)
                {
                    img [x, y, 3] = (img [x, y, 0] + img [x, y, 1] + img [x, y, 2]) / 3f;
                }
            }

            return img;
        }

        #endregion
    }
}

