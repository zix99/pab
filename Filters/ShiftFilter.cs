using System;
using pab.Lib.Image;
using pab.Lib;

namespace pab.Filters
{
    [Filter("shift", Description = "Shifts the image over time (x,y)")]
    public class ShiftFilter : IFilter
    {
        private readonly int _shiftX, _shiftY;

        public ShiftFilter (FilterArgs args)
        {
            _shiftX = args.GetArg<int> (0);
            _shiftY = args.GetArg<int> (1);
        }

        #region IFilter implementation

        public IImage Filter (Context context, IImage src, int frame)
        {
            var img = new RawImage (src.Width, src.Height);

            int offX = frame * context.FrameSize * _shiftX / context.FrameCount;
            int offY = frame * context.FrameSize * _shiftY / context.FrameCount;

            for (int y=0; y<img.Height; ++y)
            {
                for (int x=0; x<img.Width; ++x)
                {
                    for (int c=0; c<4; ++c)
                        img [x, y, c] = src [x + offX, y + offY, c];
                }
            }

            return img;
        }

        #endregion
    }
}

