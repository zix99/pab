using System;
using pab.Lib.Image;

namespace pab.Filters
{
    public abstract class AbstractPixelFilter : IFilter
    {
        private readonly string _imageName;

        public string ImageName
        {
            get{return _imageName;}
        }

        public AbstractPixelFilter (FilterArgs args)
        {
            _imageName = args[0];
        }

        #region IFilter implementation

        public IImage Filter (Context context, IImage src, int frame)
        {
            var other = context.Inputs[_imageName].GetFrame (context, frame);

            var ret = new RawImage (src.Width, src.Height);
            for (int y=0; y<src.Height; ++y)
            {
                for (int x=0; x<src.Width; ++x)
                {
                    for (int c=0; c<4; ++c)
                        ret [x, y, c] = Compute (src [x, y, c], other [x, y, c], c);
                }
            }

            return ret;
        }

        protected abstract float Compute(float baseVal, float inVal, int channel);

        #endregion
    }
}

