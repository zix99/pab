using System;
using pab.Lib.Image;

namespace pab.Filters
{
    [Filter("normmap", Description = "Turns image into a normal map based on grayscale heights")]
    public class NormalMapFilter : IFilter
    {
        private readonly float _mult = 1f;

        public NormalMapFilter (FilterArgs args)
        {
            _mult = args.GetArg<float> (0, 1f);
        }


        #region IFilter implementation
        public IImage Filter (Context context, IImage src, int frame)
        {
            var ret = new RawImage (src.Width, src.Height);

            for (int y=0; y<ret.Height; ++y)
            {
                for (int x=0; x<ret.Width; ++x)
                {
                    var s0 = (src [x, y, 0] + src [x, y, 1] + src [x, y, 2]) / 3f;
                    var sx = (src [x + 1, y, 0] + src [x + 1, y, 1] + src [x + 1, y, 2]) / 3f;
                    var sy = (src [x, y + 1, 0] + src [x, y + 1, 1] + src [x, y + 1, 2]) / 3f;

                    float px = (s0 - sx) * 0.707f + 0.3535f;
                    float py = (s0 - sy) * 0.707f + 0.3535f;
                    float pz = (float)Math.Sqrt (1f - px * px - py * py);

                    ret [x, y, 0] = px * _mult;
                    ret [x, y, 1] = py * _mult;
                    ret [x, y, 2] = pz * _mult;
                    ret [x, y, 3] = 1f;
                }
            }

            return ret;
        }
        #endregion
    }
}

