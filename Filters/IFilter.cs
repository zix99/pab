using System;

namespace pab.Filters
{
    public interface IFilter
    {
        IImage Filter(Context context, IImage src, int frame);
    }
}

