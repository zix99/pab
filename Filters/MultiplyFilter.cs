using System;

namespace pab.Filters
{
    [Filter("mult", Description = "Multiply image with source (img)")]
    public class MultiplyFilter : AbstractPixelFilter
    {
        public MultiplyFilter (FilterArgs args)
            :base(args)
        {
        }

        protected override float Compute (float baseVal, float inVal, int channel)
        {
            return baseVal * inVal;
        }

        public override string ToString ()
        {
            return string.Format ("[MultiplyFilter]");
        }
    }
}

