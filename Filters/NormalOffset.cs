using System;
using pab.Lib.Image;

namespace pab.Filters
{
    [Filter("noff", Description = "Offset an image given an input normal map (img,[power])")]
    public class NormalOffset : IFilter
    {
        private readonly string _normName;
        private readonly float _power;

        public NormalOffset (FilterArgs args)
        {
            _normName = args [0];
            _power = args.GetArg<float> (1, 8f);
        }

        #region IFilter implementation

        public IImage Filter (Context context, IImage src, int frame)
        {
            var normals = context.Inputs [_normName].GetFrame (context, frame);

            var ret = new RawImage (src.Width, src.Height);
            for (int y=0; y<src.Height; ++y)
            {
                for (int x=0; x<src.Width; ++x)
                {
                    int xOff = (int)( (normals [x, y, 0] - 0.5f) * 2f * _power);
                    int yOff = (int)( (normals [x, y, 1] - 0.5f) * 2f * _power);

                    for (int c=0; c<4; ++c)
                        ret [x, y, c] = src [x + xOff, y + yOff, c];
                }
            }

            return ret;
        }

        #endregion
    }
}

