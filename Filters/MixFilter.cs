using System;
using pab.Lib.Image;
using pab.Generators;

namespace pab.Filters
{
    [Filter("mix", Description = "Mix two images together (img,mixPercent)")]
    public class MixFilter : AbstractPixelFilter
    {
        private readonly float _mix;

        public MixFilter (FilterArgs args)
            :base(args)
        {
            _mix = args.GetArg<float> (1);
        }

        #region IFilter implementation

        protected override float Compute (float baseVal, float inVal, int channel)
        {
            return baseVal * (1f - _mix) + inVal * _mix;
        }
        #endregion

        public override string ToString ()
        {
            return string.Format ("[Mix: {0}, {1}]", this.ImageName, _mix);
        }
    }
}

