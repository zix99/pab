using System;

namespace pab.Filters
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class FilterAttribute : ProcessorAttribute
    {
        public FilterAttribute (string name)
            :base(name)
        {
        }
    }
}

