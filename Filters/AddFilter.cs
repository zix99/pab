using System;

namespace pab.Filters
{
    [Filter("add", Description = "Adds two images together. (img)")]
    public class AddFilter : AbstractPixelFilter
    {
        public AddFilter (FilterArgs args)
            :base(args)
        {
        }

        protected override float Compute (float baseVal, float inVal, int channel)
        {
            return baseVal + inVal;
        }

        public override string ToString ()
        {
            return string.Format ("[AddFilter]");
        }
    }
}

