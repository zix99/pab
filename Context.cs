using System;
using System.Collections.Generic;
using pab.Filters;
using pab.Generators;
using System.IO;
using System.Reflection;
using pab.Outputs;

namespace pab
{
    public class Context
    {
        public readonly Dictionary<string, IGenerator> Inputs = new Dictionary<string, IGenerator>(StringComparer.Ordinal);

        public IGenerator BaseInput;

        public readonly List<IFilter> Filters = new List<IFilter>();

        public readonly List<IOutput> Outputs = new List<IOutput>();

        public int FrameCount = 32;

        public int FrameSize = 256;

        public bool HelpFlag = false;

        public bool Verbose = false;

        public static Context BuildContextFromCLIArgs(string[] args)
        {
            var context = new Context ();

            try
            {
                context.BuildOptions().Parse (args);
            }
            catch(Exception e)
            {
                Console.Error.WriteLine ("Error parsing arguments: {0}", e.Message);
            }

            return context;
        }

        public void ShowHelp(TextWriter o)
        {
            o.WriteLine ("{0}: [options] <output>", AppDomain.CurrentDomain.FriendlyName);
            o.WriteLine ("  Version: {0}", Assembly.GetEntryAssembly ().GetName ().Version);
            o.WriteLine ("  By:      Chris LaPointe");
            o.WriteLine ();
            o.WriteLine ("Arguments:");
            BuildOptions().WriteOptionDescriptions (o);

            o.WriteLine ();
            Factory.PrintGenerators (o);
            o.WriteLine ();
            Factory.PrintFilters (o);
            o.WriteLine ();
            Factory.PrintOutputs (o);
        }

        private OptionSet BuildOptions()
        {
            return new OptionSet {
                {"h|help", "Show help", v => HelpFlag = v != null},
                {"v|verbose", "Verbose output", v => Verbose = v != null},
                {"frames=", "Sets the total number of frames", v => FrameCount = int.Parse(v)},
                {"size=", "Sets the size of each frame", v => FrameSize = int.Parse(v)},
                {"b|base=", "Sets the base image for each frame. Uses generators.\nFormat generator:arg1,arg2", v => BaseInput = Factory.CreateGenerator(v)},
                {"f|filter=", "Add a filter to the filter chain.\nFormat: filter:arg1,arg2", v => Filters.Add(Factory.CreateFilter(v))},
                {"i|input=", "Adds an input generator\nFormat: name=generator:arg1,arg2", AddInputGenerator},
                {"o|output=", "Sets the output filter\nFormat: type:filename", v => this.Outputs.Add(Factory.CreateOutput(v))}
            };
        }

        private void AddInputGenerator(string s)
        {
            string[] p = s.Split ('=');
            this.Inputs [p [0]] = Factory.CreateGenerator (p [1]);
        }
    }
}

