# Procedural Animation Builder (PAB)

This is a small little program I wrote that helps be build animations from a single image or a set of
images for Empeld.  It takes an input, tweens it (Either nearest, blended, etc), and runs it through
a filter chain of manipulate the pixels. It then outputs the resulting texutre.

## Building an Animation

An animation is built up of a set of things:

 - A base image
 - One or more generators
 - A chain of filters
 - An output encoder

It is processed in order that the arguments are provided to the command line. So be careful!
It's possible to give the expected frame size at the end, and to be completely ignored by
most of the filters.

## Pipeline

1. Loop through frames 0..n, where `n` is the specified number of frames, or the largest set of frames
2. Load the 'base' image for frame `n`, specified by `-b`
3. Execute each filter 0..k in the order which they appear in command line
4. Output the image via the output encoder.

### Image Set

The image set is defined by an ordered series of images within a folder.  The images
may are selected, and ordered, raw.  Do not have any other items in the folder.

### Tweener

The tweener decides how the images are stretched across the required frames.  eg. Whether
the frames are simply linear, looped, stretched out, or blended to fill.

### Filters

The real magic is in the filter chain.  There can be a set of filters with arguments that
will help determine or mix various pieces of an animation

## Arguments
The syntax of arguments is mostly `--flag operator:arg1,arg2,...`

The one exception is specifying a named extra input, which is `-i name=inputter:arg1,arg2,..`

## Examples

### Create 64 frames with filters

	pab.exe -v --frames 64 --size 128 -b seq:path/ -f add:0.5,0,0 -f alphalize -o dir:outdir/

### Create a set of normal map images from grayscale image

    pab.exe -v -b seq:path/ -f normmap -o dir:normalset/,png

### Use the above normal output to offset pixels in base image

	pab.exe -v -b water.png -i norm=seq:normalset/ -f noff:norm,16 -o dir:outdir/

### Print Help

    pab.exe -h

## License

The MIT License (MIT)

Copyright (c) 2015 Christopher LaPointe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

