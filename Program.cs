using System;
using System.Linq;
using pab.Lib.Image;

namespace pab
{
    class MainClass
    {
        public static void Main (string[] args)
        {
            var context = Context.BuildContextFromCLIArgs (args);

            if (context.Outputs.Count == 0 || context.BaseInput == null || context.HelpFlag)
            {
                context.ShowHelp (Console.Out);
                return;
            }

            context.FrameCount = context.FrameCount > 0 ? context.FrameCount : context.Inputs.Max (x => x.Value.MinFrameCount);

            if (context.Verbose) {
                Console.WriteLine ("Frames: {0} ({1}x{1})", context.FrameCount, context.FrameSize);
                Console.WriteLine ("Base :  {0}", context.BaseInput);
                Console.WriteLine ("Inputs: {0}", string.Join (", ", context.Inputs.Select (x => string.Format ("{0}={1}", x.Key, x.Value))));
                Console.WriteLine ("Filter: {0}", string.Join (" => ", context.Filters.Select (x => x.ToString ())));
                Console.WriteLine ("Output: {0}", string.Join(",", context.Outputs.Select(x => x.ToString())));
            }


            context.Outputs.ForEach (x => x.Init (context));

            for (int i=0; i<context.FrameCount; ++i)
            {
                if (context.Verbose)
                    Console.Write ("\rProcessing frame: {0}...", i);

                var baseImage = context.BaseInput.GetFrame (context, i);

                foreach(var filter in context.Filters)
                {
                    baseImage = filter.Filter (context, baseImage, i);
                }

                context.Outputs.ForEach (x => x.AddFrame (i, baseImage));
            }

            context.Outputs.ForEach (x => x.Finish ());

            if (context.Verbose)
                Console.WriteLine ("\nDone.");
        }
    }
}
