using System;

namespace pab.Generators
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class GeneratorAttribute : ProcessorAttribute
    {
        public GeneratorAttribute (string name) : base(name)
        {
        }
    }
}

