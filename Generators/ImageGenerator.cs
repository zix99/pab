using System;
using pab.Lib.Image;

namespace pab.Generators
{
    [Generator("img", Description = "Inputs a static image")]
    public class ImageGenerator : IGenerator
    {
        private readonly string _src;
        private readonly RawImage _image;

        public ImageGenerator (FilterArgs args)
        {
            _src = args[0];
            _image = RawImage.FromFile (args[0]);
        }

        #region IGenerator implementation
        public IImage GetFrame (Context context, int frame)
        {
            return _image.Resize(context.FrameSize, context.FrameSize);
        }

        public int MinFrameCount
        {
            get{return 1;}
        }

        #endregion

        public override string ToString ()
        {
            return string.Format ("[Image: {0}]", _src);
        }
    }
}

