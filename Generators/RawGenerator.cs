using System;
using pab.Lib.Image;
using System.IO;

namespace pab.Generators
{
    [Generator("raw", Description = "Inputs a saved raw-pab file")]
    public class RawGenerator : IGenerator
    {
        private readonly string _filename;
        private readonly RawImage[] _frames;

        public RawGenerator (FilterArgs args)
        {
            _filename = args [0];

            using(var s = File.OpenRead(_filename))
            {
                var reader = new BinaryReader (s);

                string magic = reader.ReadString ();
                if (magic != "PABRAW")
                    throw new Exception ("Invalid raw magic");

                int frames = reader.ReadInt32 ();

                _frames = new RawImage[frames];

                for (int i=0; i<frames; ++i)
                {
                    var frameNum = reader.ReadInt32 ();
                    _frames [frameNum] = RawImage.FromRaw (s);
                }
            }
        }

        #region IGenerator implementation

        public IImage GetFrame (Context context, int frame)
        {
            return _frames [frame % _frames.Length];
        }

        public int MinFrameCount {
            get {
                return _frames.Length;
            }
        }

        #endregion
    }
}

