using System;
using pab.Lib;
using pab.Generators.MetaImages;

namespace pab.Generators
{
    [Generator("col", Description = "Returns a solid color. Format (r,g,b[,a]) as floats")]
    public class ColorGenerator : IGenerator
    {
        private readonly Rgba _color;

        public ColorGenerator (FilterArgs args)
        {
            if (args.Count >= 3) {
                float r = args.GetArg<float> (0);
                float g = args.GetArg<float> (1);
                float b = args.GetArg<float> (2);
                float a = args.GetArg<float> (3, 1f);

                _color = new Rgba (r, g, b, a);
            }
            else if (args.Count == 1 && args[0][0] == '#')
            {
                _color = Rgba.FromHexString (args [0]);
            }
            else
            {
                throw new Exception ("Invalid color input");
            }
        }

        #region IGenerator implementation

        public IImage GetFrame (Context context, int frame)
        {
            return new ColorImage (_color, context.FrameSize, context.FrameSize);
        }

        public int MinFrameCount {
            get {
                return 0;
            }
        }

        #endregion

        public override string ToString ()
        {
            return string.Format ("[ColorGenerator: Color={0}]", _color);
        }
    }
}

