using System;
using System.Linq;
using System.IO;
using pab.Lib.Image;

namespace pab.Generators
{
    [Generator("interp", Description = "Interpolates a sequence of images")]
    public class InterpolateSequenceGenerator : IGenerator
    {
        private readonly string[] _images;

        public InterpolateSequenceGenerator (FilterArgs args)
        {
            _images = Directory.GetFiles (args [0]).OrderBy (x => x).ToArray ();
        }

        #region IGenerator implementation

        public IImage GetFrame (Context context, int frame)
        {
            float interopFrame = frame * _images.Length / (float)context.FrameCount;
            int idx = (int)interopFrame;
            float coeff = interopFrame - idx;

            //Console.WriteLine ("{0}-{1}", idx, coeff);

            var img0 = RawImage.FromFile (_images [idx % _images.Length]);
            var img1 = RawImage.FromFile (_images [(idx + 1) % _images.Length]);

            var ret = new RawImage (img0.Width, img0.Height);
            for (int i=0; i<ret.Length; ++i)
            {
                ret [i] = img0 [i] * (1f - coeff) + img1 [i] * coeff;
            }

            img0.Dispose ();
            img1.Dispose ();

            return ret.Resize(context.FrameSize, context.FrameSize);
        }

        public int MinFrameCount {
            get {
                return _images.Length;
            }
        }

        #endregion
    }
}

