using System;
using System.Linq;
using System.IO;
using pab.Lib.Image;

namespace pab.Generators
{
    [Generator("seq", Description = "Inputs a fixed sequence of images, looping if necessary")]
    public class SequenceGenerator : IGenerator
    {
        private readonly string _path;
        private readonly string[] _files;

        public SequenceGenerator (FilterArgs args)
        {
            _path = args [0];
            _files = Directory.GetFiles (args [0]).OrderBy (x => x).ToArray();
        }


        #region IGenerator implementation
        public IImage GetFrame (Context context, int frame)
        {
            return RawImage.FromFile (_files [frame % _files.Length]).Resize(context.FrameSize, context.FrameSize);
        }

        public int MinFrameCount {
            get {
                return _files.Length;
            }
        }
        #endregion

        public override string ToString ()
        {
            return string.Format ("[SequenceGenerator: {0}]", _path);
        }
    }
}

