using System;
using pab.Lib;

namespace pab.Generators.MetaImages
{
    public class ColorImage : IImage
    {
        private readonly int _width, _height;
        private readonly float[] _color;


        public ColorImage (Rgba color, int width, int height)
        {
            _color = new float[] { color.R, color.G, color.B, color.A };
            _width = width;
            _height = height;
        }

        #region IImage implementation

        public int Width {
            get {
                return _width;
            }
        }

        public int Height {
            get {
                return _height;
            }
        }

        public float this [int x, int y, int c] {
            get {
                return _color [c];
            }
        }

        #endregion
    }
}

