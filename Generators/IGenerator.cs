using System;

namespace pab.Generators
{
    public interface IGenerator
    {
        int MinFrameCount{get;}

        IImage GetFrame(Context context, int frame);
    }
}

