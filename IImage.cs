using System;

namespace pab
{
    public interface IImage
    {
        int Width{get;}

        int Height{get;}

        float this[int x, int y, int c]{get;}
    }
}

