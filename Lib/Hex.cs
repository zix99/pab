using System;
using System.Text;

namespace pab.Lib
{
    /// <summary>
    /// Hex computation helpers
    /// </summary>
    public static class Hex
    {
        private static readonly byte[] _charValues;
        private static readonly char[] _valueSymbol;

        static Hex()
        {
            _charValues = new byte[256];
            _valueSymbol = new char[16];

            //Set up 0-9
            for (int i=0; i<10; ++i)
            {
                _charValues['0' + i] = (byte)i;
                _valueSymbol[i] = (char)('0' + i);
            }

            //Set up A-F
            for (int i=0; i<6; ++i)
            {
                _charValues['A' + i] = (byte)(i + 10);
                _charValues['a' + i] = (byte)(i + 10);
                _valueSymbol[i + 10] = (char)('A' + i);
            }
        }

        /// <summary>
        /// Convert a hex digit to its corresponding decimal digit
        /// </summary>
        /// <returns>The base10 digit.</returns>
        /// <param name="c">C.</param>
        public static byte CharVal(char c)
        {
            return _charValues[c & 0xff];
        }

        /// <summary>
        /// Convert a single hex digit, into its expanded 2-digit form
        /// Eg. Interpret 0xA as 0xAA
        /// </summary>
        /// <returns>The decimal value</returns>
        /// <param name="c">The hex digit</param>
        public static byte CharValExpanded(char c)
        {
            byte v = _charValues[c & 0xff];
            return (byte)(v << 4 | v);
        }

        /// <summary>
        /// Gets the byte val of 2 hex digits
        /// </summary>
        /// <returns>The decimal value.</returns>
        /// <param name="c1">C1.</param>
        /// <param name="c2">C2.</param>
        public static byte CharVal(char c1, char c2)
        {
            return (byte)(_charValues[c1 & 0xff] << 4 | _charValues[c2 & 0xff]);
        }

        /// <summary>
        /// Convert a string sequence of Hex to its decimal form
        /// </summary>
        /// <returns>Integer</returns>
        /// <param name="s">Hex string</param>
        public static int ToInt32(string s)
        {
            int v = 0;
            for (int i=0; i<s.Length; ++i)
            {
                v = (v << 4) | _charValues[s[i] & 0xff];
            }
            return v;
        }

        /// <summary>
        /// Convert a string sequence of Hex digits to its decimal form
        /// </summary>
        /// <returns>The int64.</returns>
        /// <param name="s">Hex string</param>
        public static long ToInt64(string s)
        {
            long v = 0;
            for (int i=0; i<s.Length; ++i)
            {
                v = (v << 4) | _charValues[s[i] & 0xff];
            }
            return v;
        }

        /// <summary>
        /// Convert an integer to its hex string
        /// </summary>
        /// <returns>The hex.</returns>
        /// <param name="val">Value.</param>
        /// <param name="minLen">Minimum length.</param>
        public static string ToHex(long val, int minLen = 0)
        {
            var s = new StringBuilder();
            while(val > 0)
            {
                s.Insert(0, _valueSymbol[val & 0xf]);
                val >>= 4;
            }
            while (s.Length < minLen)
                s.Insert(0, '0');
            return s.ToString();
        }

        /// <summary>
        /// Convert an array of bytes to its hex string
        /// </summary>
        /// <returns>The hex.</returns>
        /// <param name="bytes">Bytes.</param>
        public static string ToHex(byte[] bytes)
        {
            var s = new StringBuilder();
            for (int i=0; i<bytes.Length; ++i)
            {
                s.Append(_valueSymbol[bytes[i] >> 4]); //Left bits
                s.Append(_valueSymbol[bytes[i] & 0xf]); //Right bits
            }
            return s.ToString();
        }
    }
}

