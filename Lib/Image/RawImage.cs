using System;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace pab.Lib.Image
{
    /// <summary>
    /// High-performance raw image storage and manipulation
    /// Stores data in RGBA format
    /// </summary>
    public sealed unsafe class RawImage : IImage, IDisposable
    {
        private const float ONE_OVER_255 = 1f / 255f;

        public const int CHANNELS = 4;

        public readonly int Width;
        public readonly int Height;

        private bool _disposed = false;
        private readonly float* _pixels;

        int IImage.Width
        {
            get{return this.Width;}
        }

        int IImage.Height
        {
            get{return this.Height;}
        }

        public int Length
        {
            get{return Width * Height * CHANNELS;}
        }

        public RawImage (int width, int height)
        {
            this.Width = width;
            this.Height = height;
            _pixels = (float*)Marshal.AllocHGlobal (this.Width * this.Height * CHANNELS * sizeof(float));
            Clear (0f);
        }

        ~RawImage()
        {
            this.Dispose();
        }

        public void Clear(float val)
        {
            float* ptr = _pixels + Width * Height * CHANNELS - 1;
            while(ptr >= _pixels)
            {
                *ptr = val;
                --ptr;
            }
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                Marshal.FreeHGlobal ((IntPtr)_pixels);
                _disposed = true;
                GC.SuppressFinalize (this);
            }
        }

        public float this[int idx]
        {
            get { return _pixels [idx]; }
            set { _pixels [idx] = value; }
        }

        public float this[int x, int y, int c]
        {
            get
            {
                return _pixels [(mmod (y, Height) * Width + mmod (x, Width)) * CHANNELS + c];
            }
            set
            {
                _pixels [(mmod (y, Height) * Width + mmod (x, Width)) * CHANNELS + c] = value;
            }
        }

        public Rgba this[int x, int y]
        {
            get
            {
                float* ptr = this.Ptr (x, y);
                return new Rgba (ptr [0], ptr [1], ptr [2], ptr [3]);
            }
            set
            {
                float* ptr = this.Ptr (x, y);
                ptr [0] = value.R;
                ptr [1] = value.G;
                ptr [2] = value.B;
                ptr [3] = value.A;
            }
        }

        public float* Ptr(int x, int y)
        {
            return &_pixels[ (mmod(y, Height) * Width + mmod(x, Width)) * CHANNELS ];
        }

        public static RawImage FromBitmap(Bitmap bitmap)
        {
            var pLock = bitmap.LockBits (new Rectangle (0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            try
            {
                byte* pixels = (byte*)pLock.Scan0; 
                var img = new RawImage(bitmap.Width, bitmap.Height);
                for (int i=0; i<bitmap.Width * bitmap.Height * 4; i += 4)
                {
                    img[i+0] = pixels[i+2] * ONE_OVER_255; //R
                    img[i+1] = pixels[i+1] * ONE_OVER_255 ; //G
                    img[i+2] = pixels[i+0] * ONE_OVER_255; //B
                    img[i+3] = pixels[i+3] * ONE_OVER_255; //A
                }
                return img;
            }
            finally
            {
                bitmap.UnlockBits (pLock);
            }
        }

        public static RawImage FromBitmap(Stream bitmapStream)
        {
            using (var bmp = new Bitmap(bitmapStream))
            {
                return FromBitmap (bmp);
            }
        }

        public static RawImage FromFile(string filename)
        {
            using(Stream f = File.OpenRead(filename))
            {
                return FromBitmap (f);
            }
        }

        public static RawImage FromRaw(Stream stream)
        {
            var reader = new BinaryReader (stream);
            int width = reader.ReadInt32 ();
            int height = reader.ReadInt32 ();
            int channels = reader.ReadInt32 ();

            if (channels != CHANNELS)
                throw new Exception ("Invalid channels");

            var img = new RawImage (width, height);

            byte[] bytes = new byte[width * height * channels * sizeof(float)];
            stream.Read (bytes, 0, bytes.Length);
            Marshal.Copy (bytes, 0, (IntPtr)img._pixels, bytes.Length);

            return img;
        }

        public Bitmap ToBitmap()
        {
            var bmp = new Bitmap (this.Width, this.Height, PixelFormat.Format32bppArgb);
            var bLock = bmp.LockBits (new Rectangle (0, 0, this.Width, this.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            try
            {
                byte* ptr = (byte*)bLock.Scan0;
                for (int i=0; i<this.Width * this.Height * CHANNELS; i += 4)
                {
                    ptr[i + 0] = clampConvert(_pixels[i + 2]); //B
                    ptr[i + 1] = clampConvert(_pixels[i + 1]); //G
                    ptr[i+2] = clampConvert(_pixels[i+0]); //R
                    ptr[i+3] = clampConvert(_pixels[i+3]); //A
                }
            }
            finally
            {
                bmp.UnlockBits (bLock);
            }

            return bmp;
        }

        public void Save(string filename)
        {
            using(var bmp = this.ToBitmap())
            {
                bmp.Save (filename);
            }
        }

        public void WriteRaw(Stream stream)
        {
            var writer = new BinaryWriter (stream);
            writer.Write (this.Width);
            writer.Write (this.Height);
            writer.Write (CHANNELS);

            byte[] bytes = new byte[Width * Height * CHANNELS * sizeof(float)];
            Marshal.Copy ((IntPtr)_pixels, bytes, 0, bytes.Length);
            stream.Write (bytes, 0, bytes.Length);
        }

        public void WriteRaw(string filename)
        {
            using(var stream = File.OpenWrite(filename))
            {
                WriteRaw (stream);
            }
        }

        public RawImage Clone()
        {
            var clone = new RawImage (this.Width, this.Height);
            float* cPtr = clone._pixels + this.Width * this.Height * RawImage.CHANNELS - 1;
            float* mPtr = this._pixels + this.Width * this.Height * RawImage.CHANNELS - 1;

            while(mPtr >= this._pixels)
            {
                *cPtr = *mPtr;
                --mPtr;
                --cPtr;
            }

            return clone;
        }


        private static int mmod(int i, int m)
        {
            return (i%m+m)%m;
        }

        private static byte clampConvert(float v)
        {
            float bv = v * 255f;
            return bv <= 0f ? (byte)0 : (bv >= 255f ? (byte)255 : (byte)bv);
        }
    }
}

