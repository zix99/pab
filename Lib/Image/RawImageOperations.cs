using System;
using pab.Lib.Image;

namespace pab.Lib.Image
{
    public static class RawImageOperations
    {
        public static RawImage Resize(this RawImage image, int targetWidth, int targetHeight)
        {
            if (image.Width == targetWidth && image.Height == targetHeight)
                return image.Clone ();

            int subX = Math.Max (1, image.Width / targetWidth);
            int subY = Math.Max (1, image.Height / targetHeight);

            var ret = new RawImage(targetWidth, targetHeight);
            for (int y=0; y<targetHeight; ++y)
            {
                for (int x=0; x<targetWidth; ++x)
                {

                    for (int c=0; c<RawImage.CHANNELS; ++c)
                    {
                        ret [x, y, c] = 0f;

                        for (int sY = 0; sY < subY; ++sY)
                        {
                            for (int sX = 0; sX < subX; ++sX )
                            {
                                ret[x, y, c] += image[x * image.Width / targetWidth, y * image.Height / targetHeight, c];
                            }
                        }

                        ret [x, y, c] /= (float)(subX * subY);
                    }
                }
            }
            return ret;
        }


        public static void Blit(this RawImage image, IImage src, int xOff, int yOff)
        {
            for (int y=0; y<src.Height; ++y)
            {
                for (int x=0; x<src.Width; ++x)
                {
                    for (int c=0; c<RawImage.CHANNELS; ++c)
                        image [x + xOff, y + yOff, c] = src [x, y, c];
                }
            }
        }


        public static RawImage CreateImage(this IImage image)
        {
            var raw = new RawImage (image.Width, image.Height);
            for (int y=0; y<image.Height; ++y)
            {
                for (int x=0; x<image.Width; ++x)
                {
                    for (int c=0; c<4; ++c)
                        raw [x, y, c] = image [x, y, c];
                }
            }
            return raw;
        }
    }
}

