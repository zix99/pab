using System;

namespace pab.Lib
{
    public static class MExt
    {
        public static byte Clamp(float v)
        {
            return v < 0 ? (byte)0 : (v > 255 ? (byte)255 : (byte)v);
        }


        /// <summary>
        /// Mod the way every other language does with negative numbers
        /// </summary>
        /// <param name="i">
        /// A <see cref="System.Int32"/>
        /// </param>
        /// <param name="m">
        /// A <see cref="System.Int32"/>
        /// </param>
        /// <returns>
        /// A <see cref="System.Int32"/>
        /// </returns>
        public static int mmod(int i, int m)
        {
            //This method is slightly slower in debug mode, but if it's allowed to JIT
            //it's absurdly fast.
            return (i%m+m)%m;
        }
    }
}

