using System;

namespace pab.Lib
{
    /// <summary>
    /// Class the represents a floating-point color of red, green, blue, and alpha
    /// </summary>
    public struct Rgba
    {
        private const float ONE_THIRD = 0.3333333333f;

        /// <summary>
        /// Red component
        /// </summary>
        public float R;

        /// <summary>
        /// Green component
        /// </summary>
        public float G;

        /// <summary>
        /// Blue component
        /// </summary>
        public float B;

        /// <summary>
        /// Alpha component
        /// </summary>
        public float A;

        /// <summary>
        /// Initializes a new instance of the <see cref="pluginbase.Helpers.Data.Rgba"/> struct.
        /// </summary>
        /// <param name="c">C.</param>
        public Rgba(Rgba c)
        {
            R = c.R;
            G = c.G;
            B = c.B;
            A = c.A;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="pluginbase.Helpers.Data.Rgba"/> struct.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        public Rgba(float r, float g, float b)
        {
            R = r;
            G = g;
            B = b;
            A = 1f;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="pluginbase.Helpers.Data.Rgba"/> struct.
        /// </summary>
        /// <param name="rgb">Rgb.</param>
        /// <param name="a">The alpha component.</param>
        public Rgba(Rgba rgb, float a)
        {
            R = rgb.R;
            G = rgb.G;
            B = rgb.B;
            A = a;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="pluginbase.Helpers.Data.Rgba"/> struct.
        /// </summary>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public Rgba(float r, float g, float b, float a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        /// <summary>
        /// Gets the Rgba instance of a 3 or 4-tuple byte array
        /// </summary>
        /// <returns>The bytes.</returns>
        /// <param name="bytes">Bytes.</param>
        public static Rgba FromBytes(byte[] bytes)
        {
            if (bytes.Length == 3)
            {
                return new Rgba(bytes[0] / 255f, bytes[1] / 255f, bytes[2] / 255f);
            }
            if (bytes.Length == 4)
            {
                return new Rgba(bytes[0] / 255f, bytes[1] / 255f, bytes[2] / 255f, bytes[3] / 255f);
            }
            throw new ArgumentOutOfRangeException("Expected 3 or 4 bytes");
        }

        /// <summary>
        /// Gets the Rgba from the byte-version of rgba
        /// </summary>
        /// <returns>The bytes.</returns>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public static Rgba FromBytes(byte r, byte g, byte b, byte a)
        {
            return new Rgba(r/255f, g/255f, b/255f, a/255f);
        }

        /// <summary>
        /// Gets the Rgba instaince from bytes RGB
        /// </summary>
        /// <returns>The bytes.</returns>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        public static Rgba FromBytes(byte r, byte g, byte b)
        {
            return FromBytes(r, g, b, byte.MaxValue);
        }

        /// <summary>
        /// Gets the Rgba instance from 0-255 range integers
        /// </summary>
        /// <returns>The bytes.</returns>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="a">The alpha component.</param>
        public static Rgba FromBytes(int r, int g, int b, int a)
        {
            return FromBytes((byte)r, (byte)g, (byte)b, (byte)a);
        }

        /// <summary>
        /// Gets the Rgba instance from 0-255 range integers
        /// </summary>
        /// <returns>The bytes.</returns>
        /// <param name="r">The red component.</param>
        /// <param name="g">The green component.</param>
        /// <param name="b">The blue component.</param>
        public static Rgba FromBytes(int r, int g, int b)
        {
            return FromBytes((byte)r, (byte)g, (byte)b);
        }

        /// <summary>
        /// Gets an Rgba instance from a 3,4,6,8 length hex string, including starting '#'
        /// eg #123, #1234, #11223344, #112233
        /// </summary>
        /// <returns>The hex string.</returns>
        /// <param name="hexString">Hex string.</param>
        public static Rgba FromHexString(string hexString)
        {
            if (hexString[0] != '#')
            {
                throw new FormatException("Invalid hex string");
            }   

            if (hexString.Length == 4 || hexString.Length == 5) //eg #AAA or #AAAF (RGB/RGBA)
            {
                return Rgba.FromBytes(
                    Hex.CharValExpanded(hexString[1]),
                    Hex.CharValExpanded(hexString[2]),
                    Hex.CharValExpanded(hexString[3]),
                    hexString.Length == 5 ? Hex.CharValExpanded(hexString[4]) : byte.MaxValue);
            }
            if (hexString.Length == 7 || hexString.Length == 9) //eg #AAAAAA or #AABBCCFF
            {
                return Rgba.FromBytes(
                    Hex.CharVal(hexString[1], hexString[2]),
                    Hex.CharVal(hexString[3], hexString[4]),
                    Hex.CharVal(hexString[5], hexString[6]),
                    hexString.Length == 9 ? Hex.CharVal(hexString[7], hexString[8]) : byte.MaxValue);
            }

            throw new ArgumentOutOfRangeException("Invalid hex string");
        }

        /// <summary>
        /// Convert the Rgba instance to a 8-digit hex string
        /// </summary>
        /// <returns>The hex.</returns>
        public string ToHex()
        {
            byte r = (byte)(this.R * byte.MaxValue);
            byte g = (byte)(this.G * byte.MaxValue);
            byte b = (byte)(this.B * byte.MaxValue);
            byte a = (byte)(this.A * byte.MaxValue);
            return string.Format("#{0}{1}{2}{3}", Hex.ToHex(r, 2), Hex.ToHex(g, 2), Hex.ToHex(b, 2), Hex.ToHex(a, 2));
        }

        /// <summary>
        /// Show the Rgba in a human-readable string format
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="pluginbase.Helpers.Data.Rgba"/>.</returns>
        public override string ToString()
        {
            return string.Format("[{0},{1},{2},{3}]", R, G, B, A);
        }

        #region Casting
        /// <summary>
        /// Implicit cast an Rgba instance to a 4-tuple float array
        /// </summary>
        public static implicit operator float[](Rgba color)
        {
            return new float[]{color.R, color.G, color.B, color.A};
        }

        /// <summary>
        /// Convert a 4-tuple float array to an Rgba instance
        /// </summary>
        public static explicit operator Rgba(float[] colors)
        {
            if (colors.Length != 4)
            {
                throw new IndexOutOfRangeException("Unable to convert array to RGBA, incorrect size");
            }
            return new Rgba(colors[0], colors[1], colors[2], colors[3]);
        }

        /// <summary>
        /// Convert Rgba instance to array of 4 bytes
        /// </summary>
        /// <returns>The bytes.</returns>
        public byte[] ToBytes()
        {
            return new byte[]
            {
                (byte)(R * 255f),
                (byte)(G * 255f),
                (byte)(B * 255f),
                (byte)(A * 255f)
            };
        }

        #endregion

        #region Self Operators

        /// <summary>
        /// Clamp each component of Rgba to between min and max
        /// </summary>
        /// <param name="min">Minimum.</param>
        /// <param name="max">Max.</param>
        public void Clamp(float min, float max)
        {
            R = R < min ? min : (R > max ? max : R);
            G = G < min ? min : (G > max ? max : G);
            B = B < min ? min : (B > max ? max : B);
            A = A < min ? min : (A > max ? max : A);
        }

        /// <summary>
        /// Clamp each component between 0 and 1
        /// </summary>
        public void Clamp()
        {
            Clamp(0f, 1f);
        }

        /// <summary>
        /// Clamp the Rgba instance, but in a way that scales down the rest of the colors
        /// eg, if you have 2.0,1.0,0.5,1.0, and ClampScale(1). You will get 1.0, 0.5, 0.25, 1.0
        /// The alpha component does not follow the scaling
        /// </summary>
        /// <param name="max">Max.</param>
        public void ClampScale(float max)
        {
            float mval = R > G && R > B ? R : (G > B ? G : B);
            if (mval > max)
            {
                float ratio = 1f / (mval / max);
                R *= ratio;
                G *= ratio;
                B *= ratio;
            }
            A = A > max ? max : A;
        }

        /// <summary>
        /// Clamp the Rgba instance, but in a way that scales down the rest of the colors
        /// eg, if you have 2.0,1.0,0.5,1.0, and ClampScale(1). You will get 1.0, 0.5, 0.25, 1.0
        /// The alpha component does not follow the scaling
        /// </summary>
        public void ClampScale()
        {
            ClampScale(1f);
        }

        /// <summary>
        /// Mix current color with an amount of a second color
        /// </summary>
        /// <param name="with">With.</param>
        /// <param name="amt">Amt.</param>
        public Rgba Mix(Rgba with, float amt)
        {
            return this * (1f - amt) + with * amt;
        }

        /// <summary>
        /// Get the average grayscale version of this color
        /// Alpha remains the same
        /// </summary>
        /// <value>The grayscale.</value>
        public Rgba Grayscale
        {
            get
            {
                float avg = (R + G + B) * ONE_THIRD;
                return new Rgba(avg, avg, avg, A);
            }
        }

        /// <summary>
        /// Get the average luminosity, multiplied by alpha
        /// </summary>
        /// <value>The luminosity.</value>
        public float Luminosity
        {
            get
            {
                return (R+G+B) * ONE_THIRD * A;
            }
        }

        /// <summary>
        /// Get the luminosity of just the RGB components
        /// </summary>
        /// <value>The luminosity rgb.</value>
        public float LuminosityRgb
        {
            get
            {
                return (R+G+B) * ONE_THIRD;
            }
        }

        /// <summary>
        /// Gets the human-eye corrected luminosity weights
        /// </summary>
        /// <value>The luminosity weighted.</value>
        public float LuminosityWeighted
        {
            get
            {
                return (0.299f*R + 0.587f*G + 0.114f*B) * A;
            }
        }

        /// <summary>
        /// Gets the human-eye corrected RGB components, luminosity
        /// </summary>
        /// <value>The luminosity weighted rgb.</value>
        public float LuminosityWeightedRgb
        {
            get
            {
                return 0.299f*R + 0.587f*G + 0.114f*B;
            }
        }

        #endregion

        #region Math Operators

        /// <summary>
        /// Add instances
        /// </summary>
        public static Rgba operator+(Rgba v, Rgba w)
        {
            return new Rgba(v.R + w.R, v.G + w.G, v.B + w.B, v.A + w.A);
        }

        /// <summary>
        /// Subtract components
        /// </summary>
        public static Rgba operator-(Rgba v, Rgba w)
        {
            return new Rgba(v.R - w.R, v.G - w.G, v.B - w.B, v.A - w.A);
        }

        /// <summary>
        /// Multiply each component by its counterpart
        /// </summary>
        public static Rgba operator*(Rgba v, Rgba w)
        {
            return new Rgba(v.R * w.R, v.G * w.G, v.B * w.B, v.A * w.A);
        }

        /// <summary>
        /// Multiply each component by a constant
        /// </summary>
        public static Rgba operator*(Rgba v, float c)
        {
            return new Rgba(v.R * c, v.G * c, v.B * c, v.A * c);
        }

        /// <summary>
        /// Multiply each component by a constant
        /// </summary>
        public static Rgba operator*(float c, Rgba v)
        {
            return new Rgba(v.R * c, v.G * c, v.B * c, v.A * c);
        }

        /// <summary>
        /// Divide each component by a constant
        /// </summary>
        public static Rgba operator/(Rgba v, float c)
        {
            return new Rgba(v.R / c, v.G / c, v.B / c, v.A / c);
        }

        #endregion

        #region Predefined
        /// <summary>
        /// All-zeros (0,0,0,0)
        /// </summary>
        public static readonly Rgba Zero = new Rgba(0f, 0f, 0f, 0f);

        /// <summary>
        /// Black (0,0,0,1)
        /// </summary>
        public static readonly Rgba Black = new Rgba(0f, 0f, 0f, 1f);

        /// <summary>
        /// White (1,1,1,1)
        /// </summary>
        public static readonly Rgba White = new Rgba(1f, 1f, 1f, 1f);

        /// <summary>
        /// Gray (0.5, 0.5, 0.5, 1)
        /// </summary>
        public static readonly Rgba Gray = new Rgba(0.5f, 0.5f, 0.5f, 1f);

        /// <summary>
        /// Transparent (0,0,0,0)
        /// </summary>
        public static readonly Rgba Transparent = new Rgba(0f, 0f, 0f, 0f);

        /// <summary>
        /// Red (1,0,0,1)
        /// </summary>
        public static readonly Rgba Red = new Rgba(1f, 0f, 0f, 1f);

        /// <summary>
        /// Green (0,1,0,1)
        /// </summary>
        public static readonly Rgba Green = new Rgba(0f, 1f, 0f, 1f);

        /// <summary>
        /// Blue (0,0,1,1)
        /// </summary>
        public static readonly Rgba Blue = new Rgba(0f, 0f, 1f, 1f);

        #endregion
    }
}

