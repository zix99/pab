using System;
using pab.Generators;
using System.ComponentModel;

namespace pab
{
    public class FilterArgs
    {
        private readonly string[] _args;

        public int Count
        {
            get{return _args.Length;}
        }

        public FilterArgs (string args)
        {
            _args = args.Split (',');
        }

        public string this[int i]
        {
            get{return _args[i];}
        }

        public T GetArg<T>(int i, T def = default(T))
        {
            if (i >= 0 && i < _args.Length)
            {
                try
                {
                    var converter = TypeDescriptor.GetConverter(typeof(T));
                    return (T)converter.ConvertFromString(_args[i]);
                }
                catch {}
            }
            return def;
        }
    }
}

