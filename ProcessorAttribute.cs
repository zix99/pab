using System;

namespace pab
{
    public abstract class ProcessorAttribute : Attribute
    {
        public readonly string Name;

        public string Description{ get; set;}

        protected ProcessorAttribute (string name)
        {
            Name = name;
        }
    }
}

