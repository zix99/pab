using System;
using System.Linq;
using pab.Filters;
using pab.Generators;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using pab.Outputs;

namespace pab
{
    public static class Factory
    {
        private class ProcessorItem
        {
            public ProcessorAttribute Attr;
            public Type Type;
        }

        private static readonly Dictionary<string, ProcessorItem> _generators;
        private static readonly Dictionary<string, ProcessorItem> _filters;
        private static readonly Dictionary<string, ProcessorItem> _outputs;

        static Factory()
        {
            _generators = DiscoverItems<GeneratorAttribute> ();
            _filters = DiscoverItems<FilterAttribute> ();
            _outputs = DiscoverItems<OutputAttribute> ();
        }

        private static Dictionary<string, ProcessorItem> DiscoverItems<T>()
            where T : ProcessorAttribute
        {
            return Assembly.GetExecutingAssembly ()
                .GetTypes ()
                    .Where (x => Attribute.IsDefined (x, typeof(T)))
                    .Select (x => new ProcessorItem {
                        Attr = x.GetCustomAttributes(typeof(T), true).Cast<T>().Single(),
                        Type = x
                    })
                    .ToDictionary (k => k.Attr.Name, v => v);
        }


        public static IFilter CreateFilter(string arg)
        {
            string[] parts = arg.Split (':');

            var args = new FilterArgs (parts.Length >= 2 ? parts [1] : string.Empty);

            ProcessorItem filter;
            if (_filters.TryGetValue(parts[0], out filter))
            {
                return (IFilter)Activator.CreateInstance (filter.Type, args);
            }

            return null;
        }

        public static IGenerator CreateGenerator(string arg)
        {
            string[] parts = arg.Split (':');

            if (parts.Length == 1)
                return new ImageGenerator (new FilterArgs (parts [0])); //Shorthand

            var args = new FilterArgs (parts [1]);

            ProcessorItem generator;
            if (_generators.TryGetValue(parts[0], out generator))
            {
                return (IGenerator)Activator.CreateInstance (generator.Type, args);
            }

            return null;
        }

        public static IOutput CreateOutput(string arg)
        {
            string[] parts = arg.Split (':');

            if (parts.Length == 1)
                return new SpritesheetOutput (new FilterArgs (parts [0]));

            var args = new FilterArgs (parts [1]);

            ProcessorItem output;
            if (_outputs.TryGetValue(parts[0], out output))
            {
                return (IOutput)Activator.CreateInstance (output.Type, args);
            }

            return null;
        }

        public static void PrintGenerators(TextWriter o)
        {
            PrintItemHelp (o, "Generators", _generators);
        }

        public static void PrintFilters(TextWriter o)
        {
            PrintItemHelp (o, "Filters", _filters);
        }

        public static void PrintOutputs(TextWriter o)
        {
            PrintItemHelp (o, "Outputs", _outputs);
        }

        private static void PrintItemHelp(TextWriter o, string name, IDictionary<string, ProcessorItem> items)
        {
            o.WriteLine (name + ":");
            foreach(var gen in items)
            {
                o.WriteLine ("  {0}\t{1}", gen.Value.Attr.Name, gen.Value.Attr.Description ?? string.Empty);
            }
        }
    }
}

